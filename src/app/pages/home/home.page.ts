import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {ItemUser} from '../../item.User';
import {AuthorizationService} from '../authorization/authorization.service';
import {Router} from '@angular/router';
import {GetProjectService} from './get.Project.service';
import {LocalProjectService} from './local.Project.service';
import {Storage} from '@ionic/storage';
import { Plugins } from '@capacitor/core';
import { Platform,AlertController } from '@ionic/angular';


let version: number = 0;
let loadStatus = false;
const {App} = Plugins

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  userName: any;
  projectList: any;
  itemPage = 'Строительный контроль';
  pageItemComponent;

  constructor(
    public menu: MenuController,
    private user: ItemUser,
    private removeUser: AuthorizationService,
    private router: Router,
    private projectListGet: GetProjectService,
    private storage: Storage,
    private safeProject: LocalProjectService,
    private platform: Platform,
    private alertController: AlertController
  ) {}



  logOut() {
    this.removeUser.logout();
    this.router.navigate(['/authorization']);
    this.menu.close('menu');
  }

  ngOnInit() {
    this.storage.get('userObject').then(data => {
      if (data) {
        this.userName = data.user_display_name;
      }
    });
    this.safeProject.getProject();
    this.pageItemComponent = '';
    this.platform.backButton.subscribeWithPriority(0,async () => {});
    App.addListener('backButton',()=> {
      if(this.router.url === '/home'){
        this.withAlert('хотите выйти из приложения?',()=>{
          App.exitApp();
        });
      }
    });
  }
  async withAlert(message: string, action: () => void){
    const alert = await this.alertController.create({
      message: 'хотите выйти из приложения?',
      buttons: [{
        text: 'Нет',
        role: 'cancel'
      },{
        text: 'Да',
        handler: action
      }]
    });
    await alert.present();
  }

  pageItem(item) {
    this.pageItemComponent = item;
    switch (item) {
      case 'create':
        this.itemPage = 'Создать проект';
        this.menu.close();
        break;
      case 'reports':
        this.itemPage = 'Отчёты';
        break;
      case 'help':
        this.itemPage = 'Справочная';
        break;
      case '':
        this.itemPage = 'Строительный контроль';
        break;
    }
    this.menu.close();
  }
}
