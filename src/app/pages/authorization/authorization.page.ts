import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {AuthorizationService} from './authorization.service';
import {Router} from '@angular/router';
import {ProjectArr} from '../../projectArr';


const textError = {
  logPasErr: 'Не верный логин или пароль',
  logErr: 'Не верный логин'
}
let version: number = 0;

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.page.html',
  styleUrls: ['./authorization.page.scss'],
})

export class AuthorizationPage implements OnInit {
  public regForm: FormGroup;
  public passwordEnable = true;

  validateError: boolean = false;
  user = this.api.getCurrentUser() // данные вводимого пользователя

  posts = [];

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private projectArr: ProjectArr,
    private api: AuthorizationService
  ) {
    this.user.subscribe(user => {
      if (user) {
        this.redirect('/home');
      } else {
        this.posts = [];
      }
    });
  }


  ngOnInit() {
    this.regForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
      safeUser: false
    });
  }

  textErrOutput: string = textError.logPasErr;

  newPassword() {
    this.passwordEnable = false;
    this.textErrOutput = textError.logErr;
    this.validateError = false;
    this.regForm.controls['password'].disable();
  }

  back() {
    this.passwordEnable = true;
    this.textErrOutput = textError.logPasErr;
    this.validateError = false;
    this.regForm.controls['password'].enable();
  }

  logout() {
    this.api.logout();
  }

  onSubmit(form) {
    this.api.signIn(this.regForm.value.name, this.regForm.value.password).subscribe(
      res => {
        this.redirect('/home');
      },
      err => {
        this.validateError = true;
        this.textErrOutput = textError.logPasErr;

      }
    );
  }

  redirect(url) {
    this.router.navigate([url]);
  }
}
